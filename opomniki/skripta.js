window.addEventListener('load', function() {
	//stran nalozena
	//izvedi prijavo
	
	var izvediPrijavo = function () {
		// body...
		var uporabnik = document.querySelector("#uporabnisko_ime").value; //ce imas id, z #
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden"; //ce imas class, z .
		
	}
	
	document.querySelector("#prijavniGumb").addEventListener('click', izvediPrijavo); // ob dogodku 'click' tega gumba bo klical funkcijo. Ni () pri funkciji!
	
	//Dodaj opomnik
	
	var dodajOpomnik = function() {
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value;
		var cas_opomnika = document.querySelector("#cas_opomnika").value;
		
		document.querySelector("#naziv_opomnika").value = " ";
		document.querySelector("#cas_opomnika").value = " "; //nastavi na prazna mesta
		
		var opomniki = document.querySelector("#opomniki");
		opomniki.innerHTML += " \
			<div class='opomnik senca rob'> \
				 <div class='naziv_opomnika'>" + naziv_opomnika + "</div> \
				 <div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika + "</span> sekund.</div> \
			</div>";
		
	}
	
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik); //ko nekdo klikne na dodaj, se ustavi nov opomnik
	
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	
			//TODO: 
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas == 0) {
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML; //kaksen je naziv tega opomnika
				alert("Opomnik!\n\nZadolžitev '"+naziv_opomnika+"' je potekla!"); //pojavno okno
				document.querySelector("#opomniki").removeChild(opomnik); //odstrani ta opomnik
			}
			else {
				casovnik.innerHTML = cas - 1;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
});

